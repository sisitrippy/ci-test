#!/bin/bash

START_TIME=$SECONDS

# Get the name of the current Git branch and Repository
# then ensure that only the allowed branch is buildable.
# default: 
# repository=$(basename `git rev-parse --show-toplevel`)

fullname=${BITBUCKET_REPO_FULL_NAME%:*}  # retain the part before the colon
repository=${fullname##*/}  # retain the part after the last slash

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

# repository='devel'
# branch='master'

if [[ $branch != @(development|master) ]]; then
  printf >&2 'Error...\n'
  exit 1
else
  # echo 'print env'
  # env
  # echo -e '\n\n================\n\n'
  cp templates/build-env.tmpl build-env
  sed -i 's/name/'"$repository"'/g' build-env
  sed -i 's/docker/'"$branch"'/g' build-env

  make build
fi

sleep 2

rm build-env

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo -e "\n======================================\nFinished in $ELAPSED_TIME seconds.\n"